<?php
/**
 *   
 *   @author Peter Pokrivcak
 *   @package PapApi
 *   @since Version 1.0.0
 *   
 *       
 */

 
// Turn off all error reporting if needed
//error_reporting(0);

module_load_include('php', 'uc_post_affiliate_pro', 'PapApi.class');
//require_once('PapApi.class.php');



//if (!class_exists('Drupal_Gpf_Api_IncompatibleVersionException', false)) {
 class Drupal_Gpf_Api_IncompatibleVersionException extends Exception {
  
      protected $apiLink;
	  public $message;	  
	  public $code;
  
      public function __construct($url) {
          $this->apiLink = $url. '?C=Gpf_Api_DownloadAPI&M=download&FormRequest=Y&FormResponse=Y';
          parent::__construct('Version of API not corresponds to the Application version. Please <a href="' . $this->apiLink . '">download latest version of API</a>.', 0);
      }
      
      public function getApiDownloadLink() {
          return $this->apiLink;
      }
  
  }

//} //end Drupal_Gpf_Api_IncompatibleVersionException




if (!class_exists('Drupal_Gpf_Api_Session', false)) { 

 class Drupal_Gpf_Api_Session extends Gpf_Api_Session { 

	/**
  	 * @param $latestVersion
  	 * @throws Drupal_Gpf_Api_IncompatibleVersionException
  	 */
  	public function checkApiVersion(Gpf_Rpc_Form $form) {
  		if ($form->getFieldValue('correspondsApi') === Gpf::NO) {
			
			$this->url = variable_get('merchant_url_prefix','http://demo.qualityunit.com/pax4/') . 'scripts/server.php';
  		    //throw new Drupal_Gpf_Api_IncompatibleVersionException($this->url);
			$errors = new Drupal_Gpf_Api_IncompatibleVersionException($this->url);
			if($errors->code > 0 ) {
			drupal_set_message(t("API call error: !error", array('!error' => $errors->message)), 'error');
			}
				
  		}
  	}
 
 } 
 
}
